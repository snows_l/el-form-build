/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-17 14:36:12
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-10-17 14:41:19
 * @FilePath: /el-form-build/vue.config.js
 */
const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false
});
