/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-17 14:36:12
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-10-17 14:39:36
 * @FilePath: /el-form-build/src/router/index.js
 */
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/form-build'
  },
  {
    path: '/form-build',
    name: 'FormBuild',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/tool/formBuild')
  }
];

const router = new VueRouter({
  routes
});

export default router;
